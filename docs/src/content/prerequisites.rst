Prerequisites
==============================

The following prerequisites must be installed to use monitoring toolkit:

- python >= 3.7
- git

Installation
-----------------

Currently, the way to install this toolkit is to git clone the repository and then install it. Prerequisite for installation is a working CMake installation with version >= 3.17.5.

To set up the repository and get configuration files:

::

   git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon.git
   cd ska-sdp-perfmon

To install all the required dependencies

::

   pip3 install --user -r requirements.txt

And finally, install the package using

::

  python3 setup.py install

Another way is to use ``--editable`` option of ``pip`` installation as follows:

::

  pip install "--editable=git+https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon.git@main#egg=ska-sdp-perfmon"

This command clones the git repository and runs ``python3 setup.py develop``. This line can be directly added to the ``conda`` environment files.
