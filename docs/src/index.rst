.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

Welcome to documentation of SDP Workflows Performance Monitoring Tool
================================================================================

This documentation contains the overview of the SDP workflow performance monitoring tool (SDP Perfmon), instructions to install and usage examples.

.. toctree::
  :maxdepth: 2

  ../content/context
  ../content/prerequisites
  ../content/usage

API Documentation
====================================

The following sections provide the API documentation of the source code of SDP workflow performance monitoring toolkit.

.. toctree::
  :maxdepth: 1

  ../api/api


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
