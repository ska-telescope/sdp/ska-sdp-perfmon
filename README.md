# SDP Workflow Performance Monitoring Tools

This toolkit monitors the CPU related performance metrics and log them into different file formats like excel, HDF5. In its current implementation, the jobs must be submitted using SLURM/PBS/OAR for the toolkit to work.

More information on how to install, use the toolkit can be found in the [documentation](https://developer.skao.int/projects/ska-sdp-perfmon/en/latest/?badge=latest).

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-perfmon/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-perfmon/en/latest/?badge=latest)

---
**NOTE**

This repository is migrated and then refactored from [ska-sdp-monitor-cpu-metrics](https://gitlab.com/ska-telescope/platform-scripts/-/tree/master/ska-sdp-monitor-cpu-metrics) in [platform-scripts](https://gitlab.com/ska-telescope/platform-scripts) repository.

---
