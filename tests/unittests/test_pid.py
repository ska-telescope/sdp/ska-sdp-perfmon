#!/usr/bin/env python3
""" This script does the unit tests for the perf related functions"""

import os
import sys
import shutil
import unittest
import yaml
import psutil
import perfmon

from unittest import mock
from unittest.mock import patch

from perfmon.common.pid import GetJobPid
from perfmon.common.pid.pid import if_script_name_in_chilren


class TestPID(unittest.TestCase):
    """Class that tests PID extraction class"""

    def setup(self):
        """Set up config"""

        self.config = {
            'scheduler': 'SLURM',
            'job_id': 1,
            'node_name': 'test.host',
            'master_node': 'test.host',
            'launcher': 'mpirun',
            'script_name': ['perfmon'],
        }

        os.environ['USER'] = 'test'

    # @patch.object(os, 'getpid')
    # def test_config(self, mock_getpid):
    #     """Test config"""
    #
    #     self.setup()
    #     job_pid = GetJobPid(self.config)
    #
    #     mock_getpid.return_value = 1
    #
    #     assert job_pid.user_name == 'test'

    def test_if_script_name_in_chilren_true(self):
        """Test if_script_name_in_chilren method"""

        self.setup()
        job_pid = GetJobPid(self.config)

        self.assertTrue(
            if_script_name_in_chilren(self.config['script_name'], ['perfmon', 'matmul', 'stream'])
        )

    def test_if_script_name_in_chilren_false(self):
        """Test if_script_name_in_chilren method"""

        self.setup()
        job_pid = GetJobPid(self.config)

        self.assertFalse(
            if_script_name_in_chilren(self.config['script_name'], ['matmul', 'stream'])
        )


if __name__ == '__main__':
    unittest.main(verbosity=2)
