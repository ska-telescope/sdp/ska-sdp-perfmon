#!/usr/bin/env python3
""" This script does the unit tests for the perf related functions"""

import unittest

from unittest.mock import patch

from perfmon.common.perf import get_mem_bw_event
from perfmon.common.perf import get_working_perf_events
from perfmon.common.perf import llc_cache_miss_perf_event
from perfmon.common.perf import perf_event_list
from perfmon.exceptions import PerfEventListNotFoundError


class TestPerf(unittest.TestCase):
    """Class that tests perf related functions"""

    @patch('perfmon.common.perf.get_cpu_spec')
    def test_get_mem_bw_event_not_found(self, mock_get_cpu_spec):
        """Test memory bandwidth event"""

        # Patch return value to return unknown vendor
        mock_get_cpu_spec.return_value = (
            'Dummy', 'Dummy'
        )

        assert get_mem_bw_event() == ''

    @patch('perfmon.common.perf.get_cpu_spec')
    def test_get_working_perf_events_not_found(self, mock_get_cpu_spec):
        """Test get_all_perf_events"""

        # Patch return value to return unknown vendor
        mock_get_cpu_spec.return_value = (
            'Dummy', 'Dummy'
        )

        assert get_working_perf_events() == ({}, {})

    def test_llc_cache_miss_perf_event(self):
        """Test llc_cache_miss_perf_event"""

        assert llc_cache_miss_perf_event('GenuineIntel', 'Dummy') == ''

    def test_llc_cache_miss_perf_event_exception(self):
        """Test llc_cache_miss_perf_event"""

        assert llc_cache_miss_perf_event('Dummy', 'Dummy') == ''

    def test_perf_event_list_exception(self):
        """Test perf_event_list exception raising"""

        with self.assertRaises(PerfEventListNotFoundError):
            perf_event_list('Dummy')


if __name__ == '__main__':
    unittest.main(verbosity=2)
