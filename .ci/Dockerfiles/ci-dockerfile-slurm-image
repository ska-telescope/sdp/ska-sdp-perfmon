FROM debian:latest

RUN apt update && apt install -y --no-install-recommends \
                                     sudo \
                                     nano \
                                     git \
                                     git-lfs \
                                     hostname \
                                     hwloc \
                                     wget \
                                     tree \
                                     cmake \
                                     make \
                                     gcc \
                                     pkgconf \
                                     lcov \
                                     libopenmpi-dev \
                                     munge \
                                     libmunge-dev \
                                     slurm \
                                     slurmctld \
                                     slurmd \
                                     libconfig-dev \
                                     libspdlog-dev \
                                     doxygen \
                                     graphviz \
                                     python3 \
                                     python3-pip \
                                     python3-dev \
                                     fftw-dev \
                                     libhdf5-dev \
                                     liblzo2-dev \
                                     libblosc-dev \
                                     libbz2-dev

COPY requirements.txt /ci/
COPY requirements-tests.txt /ci/

# Install pip dependencies for tests and benchmark suite
RUN pip3 install wheel numpy
RUN pip3 install -r /ci/requirements.txt -r /ci/requirements-tests.txt

# Install GO
RUN export VERSION=1.16.2 OS=linux ARCH=amd64 && \
    wget https://golang.org/dl/go$VERSION.$OS-$ARCH.tar.gz && \
    rm -rf /usr/local/go && tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz && \
    rm go$VERSION.$OS-$ARCH.tar.gz && \
    echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.bashrc && \
    echo 'export GOPATH=${HOME}/go' >> ~/.bashrc && \
    export PATH=$PATH:/usr/local/go/bin

# Install Singularity
RUN export GOPATH=${HOME}/go && \
    export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin && \
    export VERSION=3.7.4 && \
    wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz && \
    tar -xzf singularity-${VERSION}.tar.gz && \
    rm -rf singularity-${VERSION}.tar.gz && \
    cd singularity && \
    ./mconfig && \
    make -C ./builddir && \
    make -C ./builddir install

# RUN echo 'export PATH=/usr/lib64/openmpi/bin:${PATH}' >> ~/.bashrc
# RUN echo 'source /etc/profile.d/modules.sh' >> ~/.bashrc

COPY .ci/Dockerfiles/docker-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["bash"]
