#!/bin/bash

set -x

_run_setup() {
    sudo mkdir /run/munge
    sudo chown munge:munge /run/munge
    sudo mkdir /var/run/slurm
    sudo mkdir /var/spool/slurm
}

_munge_setup() {
    #create-munge-key -f > /dev/null
    dd if=/dev/urandom bs=1 count=1024 > /etc/munge/munge.key
    sudo -u munge munged
}

# TODO: Create slurm user
_slurm_setup() {

    cat > /etc/slurm/slurm.conf <<EOF
#
# See the slurm.conf man page for more information.
#

SlurmctldHost=$(hostname -s)
AuthType=auth/munge                                     # auth_none.so not available on Fedora
EnforcePartLimits=NO
MpiDefault=pmix
ProctrackType=proctrack/pgid                            # setup with cgroups possible in container?
ReturnToService=0
SlurmctldPidFile=/var/run/slurm/slurmctld.pid
SlurmctldPort=6817
SlurmdPidFile=/var/run/slurm/slurmd.pid
SlurmdPort=6818
SlurmdSpoolDir=/var/spool/slurm/d
SlurmUser=root
SlurmdUser=root
StateSaveLocation=/var/spool/slurm/ctld
SwitchType=switch/none
TaskPlugin=task/none

# TIMERS
InactiveLimit=0
KillWait=30
MinJobAge=300
SlurmctldTimeout=120
SlurmdTimeout=300
Waittime=0

# SCHEDULING
SchedulerType=sched/backfill
SelectType=select/linear

# JOB PRIORITY

# TODO: Cleanup needed when setting up slurmdbd.
# LOGGING AND ACCOUNTING
AccountingStorageType=accounting_storage/none
ClusterName=sid                                         # sid = Slurm in Docker
JobCompType=jobcomp/none
JobAcctGatherFrequency=30
JobAcctGatherType=jobacct_gather/none
SlurmctldDebug=3
SlurmdDebug=3
MailProg=/bin/true                                      # so the daemons don't complain

# COMPUTE NODES
NodeName=$(hostname -s) Sockets=1 CoresPerSocket=$(nproc) ThreadsPerCore=1 State=UNKNOWN
PartitionName=sidp Nodes=$(hostname -s) Default=YES MaxTime=INFINITE State=UP
EOF

    slurmctld
    slurmd
}

_module_setup() {

  source /etc/profile.d/modules.sh

}

_main() {
    _run_setup
    _munge_setup
    _slurm_setup
    # _module_setup

    if [[ "${1:0:1}" = "-" ]]; then
        echo "Please pass a program name to the container!"
        exit 1
    else
        exec "$@"
    fi
}

_main "$@"
