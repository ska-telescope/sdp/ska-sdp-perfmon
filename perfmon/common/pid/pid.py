"""This module contains class for detecting process PIDs for various schedulers"""

import logging
from importlib import import_module
import psutil

from perfmon.common.utils.dispatch import Dispatch
from perfmon.common.utils.process import get_proc_info
from perfmon.exceptions import JobPIDNotFoundError

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301

_scheduler_mods = {
    'slurm': 'perfmon.common.pid.slurm',
    'oar': 'perfmon.common.pid.oar_pbs',
    'pbs': 'perfmon.common.pid.oar_pbs',
}


def if_script_name_in_chilren(script_names, child_proc_names):
    """Return bool if script name in the list of names of children"""
    return any(b for sn in script_names for b in map(sn.__contains__, child_proc_names))


@Dispatch
def find_job_pid(name, scheduler):
    """This method returns the job pid by iterating over all pids of the user and finding the
    parent process"""

    if name == "naive":
        _log.info("Job-ID via naive method -> Ignoring Job-ID")
        return []

    # List of job pids of mpirun/orted/slurmstepd/mpiexec/hydra_pmi_proxy
    launcher_pids = []

    # List of MPI process daemons for different schedulers
    mpi_daemons = [
        'mpirun',
        'orted',
        'slurmstepd',
        'mpiexec',
        'hydra_pmi_proxy',
        'psid',
    ]

    # Iterate over all processes
    for proc in psutil.process_iter():
        if proc.username() == scheduler['user_name'] and proc.name() in mpi_daemons:
            launcher_pids.append(proc.pid)

    found_pids = []
    for pid in launcher_pids:
        child_proc_names = []
        proc = get_proc_info(pid)
        if proc:
            for p in proc.children():
                child_proc_names.append(p.name())
            _log.debug('Children processes names are %s' % child_proc_names)
            # If children do not have script name that is main job
            if not if_script_name_in_chilren(
                scheduler['script_names'], child_proc_names
            ):
                found_pids.append(pid)

    if found_pids:
        return found_pids
    else:
        # If pid is not found, raise an exception
        _log.error('Could not find job PID by naive method.')
        raise JobPIDNotFoundError('Could not find job PID.')


@find_job_pid.register('slurm')
def register_slurm(name, scheduler):
    """Register SLURM implementation"""
    mod = import_module(_scheduler_mods[name])
    return getattr(mod, 'register_slurm')(scheduler)


@find_job_pid.register('oar')
def register_oar(name, scheduler):
    """Register OAR implementation"""
    mod = import_module(_scheduler_mods[name])
    return getattr(mod, 'register_pbs_oar')(scheduler)


@find_job_pid.register('pbs')
def register_pbs(name, scheduler):
    """Register PBS implementation"""
    mod = import_module(_scheduler_mods[name])
    return getattr(mod, 'register_pbs_oar')(scheduler)
