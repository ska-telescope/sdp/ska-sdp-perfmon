"""Utility functions for psutil process finder"""

import logging
import psutil

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_proc_info(pid):
    """Convenient wrapper around psutil.Process to catch exceptions"""

    try:
        return psutil.Process(pid)
    except (psutil.AccessDenied, psutil.NoSuchProcess):
        return None


def find_procs_by_name(name):
    """Return a list of processes matching 'name'

    Args:
        name (str): name of the process to find
    Returns:
        list: List of psutil objects
    """
    ls = []
    for p in psutil.process_iter(['name']):
        if p.info['name'] == name:
            ls.append(p)

    return ls


def proc_if_running(procs):
    """Check if all processes are running and returns a False if all of them are terminated

    Args:
        procs (list): List of psutil process objects
    Returns:
        bool: Running status of the processes
    """

    # if procs contains no processes, the processes were found using the naive method -> Skip
    if len(procs) == 0:
        return True

    # Get status of all processes
    all_proc_stats = [p.is_running() for p in procs]

    return any(all_proc_stats)
