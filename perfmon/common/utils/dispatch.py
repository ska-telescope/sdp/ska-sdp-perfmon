"""Dispatcher class"""

import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class Dispatch:
    """Value-dispatch function decorator.

    Transforms a function into a value-dispatch function, which can have
    different behaviors based on the value of its first argument.

    See: http://hackwrite.com/posts/learn-about-python-decorators-by-writing-a-function-dispatcher
    """

    def __init__(self, func):
        self.func = func
        self.registry = {}

    def dispatch(self, value):
        try:
            return self.registry[value]
        except KeyError:
            return self.func

    def register(self, value, func=None):
        if func is None:
            return lambda f: self.register(value, f)
        self.registry[value] = func
        return func

    def __call__(self, *args, **kw):
        return self.dispatch(args[0])(*args, **kw)


if __name__ == '__main__':

    @Dispatch
    def get_job_pid(c):
        print('I will get job PID looking at all user space PIDs')

    @get_job_pid.register('slurm')
    def register_schedulers(sc, c):
        from importlib import import_module

        mod = import_module('perfmon.common.utils.test')
        getattr(mod, 'register_slurm')(23)

    get_job_pid('slurm', {'sc': 1})
    get_job_pid('oar')
