"""Class to create pdf file"""

import datetime
import logging
import fpdf
import PIL

logging.getLogger('fpdf').setLevel(logging.WARNING)
logging.getLogger('PIL').setLevel(logging.WARNING)

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class PDF(fpdf.FPDF):
    """custom PDF class that inherits from the FPDF"""

    def __init__(self, config):

        super().__init__()
        # A4 size
        self.width = 210
        self.height = 297

        self.start_y = 18
        self.eff_height = self.height - self.start_y

        self.config = config.copy()

    def header(self):
        """This method defines header of the pdf"""

        self.set_font('Times', 'B', 10)
        self.cell(0, 5, 'Job ID: {:<30} User: {:<30} # Nodes: '
                        '{:<30} Date: {:<30}'.
                  format(self.config['job_id'], self.config['user'],
                         self.config['num_nodes'],
                         datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')),
                  1, 0, 'C')
        self.ln(20)

    def footer(self):
        """This method defines footer of the pdf"""

        # Page numbers in the footer
        self.set_y(-15)
        self.set_font('Times', 'I', 8)
        self.set_text_color(128)
        self.cell(0, 10, 'Page ' + str(self.page_no()), 0, 0, 'C')

    def page_body(self, images):
        """This method defines body of the pdf"""

        # Determine how many plots there are per page and set positions
        # and margins accordingly
        for im, image in enumerate(images):
            if im == 0:
                self.image(image, 15, self.start_y, self.width - 30)
            else:
                self.image(image, 15,
                           self.start_y +
                           im * (self.eff_height / len(images)) - 7,
                           self.width - 30)

    def print_page(self, images):
        """This method add an empty pages and populates with images/text"""

        # Generates the report
        self.add_page()
        self.page_body(images)
