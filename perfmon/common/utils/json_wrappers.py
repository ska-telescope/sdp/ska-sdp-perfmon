"""Utility functions for manipulating json files"""

import os
import logging
import json

from perfmon.common.utils.utilities import merge_dicts

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def load_json(filename):
    """This function loads json file and return dict

    Args:
        filename (str): Name of the file to load
    Returns:
        dict: File contents as dict
    """

    with open(filename, 'r') as json_file:
        content = json.load(json_file)

    return content


def write_json(content, filename):
    """This function writes json content to a file

    Args:
        content (dict): Dict to write into JSON format
        filename (str): Name of the file to load
    """

    with open(filename, 'w') as json_file:
        json.dump(content, json_file, indent=4, sort_keys=True)


def dump_json(content, filename):
    """This function appends data to an existing json content. It creates a new file if no
    existing file found.

    Args:
        content (dict): Dict to write into JSON format
        filename (str): Name of the file to load
    """

    if os.path.isfile(filename):
        existing_data = load_json(filename)
        updated_dict = merge_dicts(existing_data, content)
    else:
        updated_dict = content

    write_json(updated_dict, filename)
