"""This module contains class to generate job report"""

import os
import logging

from perfmon.common.utils.json_wrappers import load_json
from perfmon.common.utils.pdf import PDF

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class GenReport(object):
    """This class does all the post monitoring steps like making plots and generating reports"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

        # Initialise plot related parameters
        self.initialise_plot_per_page()

    def initialise_plot_per_page(self):
        """Initialises plot related parameters"""

        # Define plots per page
        self.plots_per_page = [
            [
                'Average_CPU_Load_all_nodes.png',
                'Average_Memory_all_nodes.png',
                'Average_Memory_Bandwidth_(Read)_all_nodes.png',
            ],
            [
                'Total_Output_Traffic_all_nodes.png',
                'Total_Input_Traffic_all_nodes.png',
                'Total_Packets_Output_all_nodes.png',
                'Total_Packets_Input_all_nodes.png',
            ],
            [
                'Total_Interconnect_Output_Traffic_all_nodes.png',
                'Total_Interconnect_Input_Traffic_all_nodes.png',
                'Total_Interconnect_Packets_Output_all_nodes.png',
                'Total_Interconnect_Packets_Input_all_nodes.png',
            ],
            [
                'Total_RAPL_Package_Power_all_nodes.png',
                'Total_RAPL_DRAM_Power_all_nodes.png',
                'Total_RAPL_Core_Power_all_nodes.png',
                'Total_RAPL_Uncore_Power_all_nodes.png',
            ],
            [
                'CPU_Load_per_node.png',
                'Memory_per_node.png',
                'Memory_Bandwidth_(Read)_per_node.png',
            ],
            [
                'Output_Traffic_per_node.png',
                'Input_Traffic_per_node.png',
                'Packets_Output_per_node.png',
                'Packets_Input_per_node.png',
            ],
            [
                'Interconnect_Output_Traffic_per_node.png',
                'Interconnect_Input_Traffic_per_node.png',
                'Interconnect_Packets_Output_per_node.png',
                'Interconnect_Packets_Input_per_node.png',
            ],
            [
                'RAPL_Package_Power_per_node.png',
                'RAPL_DRAM_Power_per_node.png',
                'RAPL_Core_Power_per_node.png',
                'RAPL_Uncore_Power_per_node.png',
            ],
        ]

    def create_job_report(self, content):
        """Create a job report using FPDF module"""

        # Number of nodes in the job
        num_nodes = len(content['host_names'])
        # Get username of the job
        user_name = os.environ['USER']

        # Add them to the config file
        self.config['user'] = user_name
        self.config['num_nodes'] = num_nodes

        # Initiate the class
        pdf = PDF(self.config)

        # Add each page to pdf
        for elem in self.plots_per_page:
            image_list = []
            for image in elem:
                image_path = os.path.join(self.config['plot_dir'], image)
                if os.path.isfile(image_path):
                    image_list.append(image_path)
            pdf.print_page(image_list)

        # Finally save pdf
        pdf.output(self.config['report_path'], 'F')

    def go(self):
        """Entry point for creating report"""

        _log.info('Creating job report...')

        # Load CPU metric data
        content = load_json(os.path.join(self.config['data_dir'], 'cpu_metrics.json'))

        if content['host_names']:
            self.create_job_report(content)
            _log.info('Job report generated')
        else:
            _log.warning('No metric data found. Skipping job report generation.')
