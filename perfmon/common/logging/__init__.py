"""This file contains initialisation functions for logging"""

import os
import sys
import logging
import socket

# pylint: disable=E0401,W0201,C0301


class HostnameFilter(logging.Filter):
    hostname = socket.gethostname()

    def filter(self, record):
        record.hostname = HostnameFilter.hostname
        return True


def logger_config(global_config):
    """
    shortcut method for initializing logging

    Args:
        global_config (dict): Dict containing all the configuration info

    Returns:
        logger object: Logger initiated based on config passed
    """

    # Enable logging
    logger = logging.getLogger()

    # Configure logging
    # Log verbosity
    if global_config['verbose']:
        log_level = logging.DEBUG
        log_file = f"ska_sdp_monitoring_metrics_{global_config['host_name']}.log"
    else:
        log_level = logging.INFO
        log_file = 'ska_sdp_monitoring_metrics.log'

    # Set log level
    logger.setLevel(log_level)

    # Log format
    log_formatter = logging.Formatter(
        '%(asctime)s | %(levelname)s | %(hostname)s | %(name)s:%(funcName)s '
        '| %(lineno)d | %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
    )

    # Handler to write logs to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.addFilter(HostnameFilter())
    stream_handler.setFormatter(log_formatter)
    stream_handler.setLevel(log_level)

    # Handler to write logs to file
    LOG_PATH = os.path.join(global_config['save_dir'], log_file)
    file_handler = logging.FileHandler(LOG_PATH)
    file_handler.addFilter(HostnameFilter())
    file_handler.setFormatter(log_formatter)
    file_handler.setLevel(log_level)

    # Select loggers
    logger.addHandler(file_handler)
    # logger.addHandler(stream_handler)

    return logger
