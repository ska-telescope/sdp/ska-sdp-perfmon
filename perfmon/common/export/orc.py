""""This package contains functions to export metric data in ORC format"""

import os
import logging
import pyarrow
import pyarrow.orc as orc  # This prevents: AttributeError: module 'pyarrow' has no attribute 'orc'

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def orc_exporter(config, df_dict):
    """This method exports the dataframe data into ORC format"""

    # Initialise store
    for metric, df in df_dict.items():
        if df.empty:
            _log.debug('No %s data found to export to excel. Skipping' % metric)
            continue
        file_name = os.path.join(config['save_dir'], ".".join([metric, 'orc']))
        table = pyarrow.Table.from_pandas(
            df,
            preserve_index=False,
        )
        orc.write_table(table, file_name)
