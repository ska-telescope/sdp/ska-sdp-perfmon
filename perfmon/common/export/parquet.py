""""This package contains functions to export metric data in Parquet format"""

import os
import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def parquet_exporter(config, df_dict):
    """This method exports the dataframe data into parquet format"""

    # Initialise store
    for metric, df in df_dict.items():
        if df.empty:
            _log.debug('No %s data found to export to excel. Skipping' % metric)
            continue
        file_name = os.path.join(config['save_dir'], ".".join([metric, 'parquet']))
        df.to_parquet(
            file_name,
        )
