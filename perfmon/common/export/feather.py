""""This package contains functions to export metric data in feather format"""

import os
import logging
import feather


_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def feather_exporter(config, df_dict):
    """This method exports the dataframe data into feather format"""

    # Initialise store
    for metric, df in df_dict.items():
        if df.empty:
            _log.debug('No %s data found to export to excel. Skipping' % metric)
            continue
        file_name = os.path.join(config['save_dir'], ".".join([metric, 'feather']))
        feather.write_dataframe(
            df,
            file_name,
        )
