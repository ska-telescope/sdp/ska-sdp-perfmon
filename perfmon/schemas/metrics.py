"""This is schema for metrics data"""


cpu_data_schema = {
    'time_stamps': [],
    'num_threads': [],
    'cpu_time': [],
    'cpu_percent': [],
    'cpu_percent_sys': [],
    'memory_percent': [],
    'memory_info': {'rss': [], 'vms': [], 'shared': []},
    'memory_full_info': {'uss': [], 'swap': []},
    'io_counters': {'read_count': [], 'write_count': [], 'read_bytes': [], 'write_bytes': []},
    'net_io_counters': {'bytes_sent': [], 'bytes_recv': [], 'packets_sent': [], 'packets_recv': []},
    'num_fds': [],
}


nv_gpu_data_schema = {
    'memory_used': {
        'gpu_memory': [],
        'bar1_memory': [],
    },
    'utilization_rates': {'gpu': [], 'memory': []},
    'clock_info': {
        'graphics': [],
        'sm': [],
        'memory': [],
    },
    'ecc_errors': {
        'sp': [],
        'dp': [],
    },
    'fan_speed': [],
    'temperature': [],
    'power_usage': [],
    'encoder_util': [],
    'decoder_util': [],
    'num_procs': [],
    'pci_thgpt': {
        'send': [],
        'recv': [],
    },
    'violation': [],
    'libpowersensor_metrics': {
        'joules': [],
        'watt': [],
    }
}


# Libpowersensor currently supports measurement of Joules and Watt.
amd_gpu_data_schema = {
    'joules': [],
    'watt': [],
}