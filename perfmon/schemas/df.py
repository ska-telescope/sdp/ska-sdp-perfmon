"""This is schema for dataframe"""


metric_header_names = {
    'host_names': 'Host',
    'time_stamps': 'Timestamps',
    'cpu_percent': 'CPU Percent [%]',
    'cpu_percent_sys': 'System wide CPU Percent [%]',
    'cpu_time': 'CPU Time [s]',
    'memory_full_info': {
        'swap': 'Swap memory [bytes]',
        'uss': 'USS memory [bytes]',
    },
    'memory_info': {
        'rss': 'RSS memory [bytes]',
        'shared': 'Shared memory [bytes]',
        'vms': 'VMS memory [bytes]',
    },
    'memory_percent': 'System memory usage [%]',
    'io_counters': {
        'read_bytes': 'IO read [bytes]',
        'write_bytes': 'IO write [bytes]',
        'read_count': 'IO read counts',
        'write_count': 'IO write counts',
    },
    'net_io_counters': {
        'bytes_recv': 'Network recv data [bytes]',
        'bytes_sent': 'Network sent data [bytes]',
        'packets_recv': 'Network recv packets',
        'packets_sent': 'Network sent packets',
    },
    'num_fds': '# file descriptors',
    'num_threads': '# threads',
}

ib_metric_header_names = {
    'ib_io_counters': {
        'port_rcv_data': 'IB recv data [bytes]',
        'port_xmit_data': 'IB xmit data [bytes]',
        'port_rcv_packets': 'IB recv packets',
        'port_xmit_packets': 'IB xmit packets',
    },
}

perfcounters_header_names = {
    'host_names': 'Host',
    'time_stamps': 'Timestamps',
    'hardware_events': {
        'branch-misses:u': 'Branch misses',
        'branches:u': '# Branches',
        'cache-misses:u': 'Cache misses',
        'cache-references:u': 'Cache references',
        'cycles:u': 'Cycles',
        'instructions:u': 'Instructions',
    },
    'software_events': {
        'context-switches:u': 'Context switches',
        'cpu-migrations:u': 'CPU migrations',
    },
}

nv_gpu_metric_header_names = {
    'host_names': 'Host',
    'time_stamps': 'Timestamps',
    'memory_used': {
        'gpu_memory': 'GPU memory used [bytes]',
        'bar1_memory': 'BAR1 memory used [bytes]',
    },
    'utilization_rates': {
        'gpu': 'GPU Percent [%]',
        'memory': 'Memory Percent [%]',
    },
    'clock_info': {
        'graphics': 'Graphics clock speed [Mhz]',
        'sm': 'SM clock speed [Mhz]',
        'memory': 'Memory clock speed [Mhz]',
    },
    'ecc_errors': {
        'sp': '# SP ECC errors',
        'db': '# DP ECC errors',
    },
    'fan_speed': 'Fan speed [rpm]',
    'temperature': 'Device temperature [C]',
    'power_usage': 'Power usage [mW]',
    'encoder_util': 'Encoder percent [%]',
    'decoder_util': 'Decoder percent [%]',
    'num_procs': '# processes',
    'pci_thgpt': {
        'send': 'PCIe send throughput [KB/s]',
        'recv': 'PCIe recv throughput [KB/s]',
    },
    'violation': 'GPU throttling time [nano sec]',
}
