"""This file contains class that setup and tear down monitoring"""

import os
import sys
import logging
import yaml

from perfmon.core.setup.setup_dirs import SetUpDirs
from perfmon.core.setup.setup_scheduler import SetUpScheduler
from perfmon.core.setup.setup_ipc import SetUpIPC
from perfmon.core.teardown import MergeFiles

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class SetUpMonitor(object):
    """Setting up monitoring"""

    def __init__(self, config):
        """Initialise class"""

        self.config = config

    def go(self):
        """Entry point to the class"""

        # Add scheduler config
        setup_scheduler = SetUpScheduler(self.config)
        self.config = setup_scheduler.go()

        # Create dirs
        setup_dirs = SetUpDirs(self.config)
        self.config = setup_dirs.go()

        # Create IPC file
        setup_ipc = SetUpIPC(self.config)
        self.config = setup_ipc.go()

        return self.config


class PostMonitoring(object):
    """This class performs post monitoring steps"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config.copy()

    def dump_config(self):
        """Dump config files (for debugging)"""

        # Create a directory to save config files
        os.makedirs(os.path.join(self.config['save_dir'], 'configs'), exist_ok=True)
        # Save running config
        with open(
            os.path.join(
                self.config['save_dir'],
                'configs',
                '_'.join([self.config['host_name'], 'run_config.yml']),
            ),
            'w',
        ) as conf_file:
            yaml.dump(self.config, conf_file)

    def go(self):
        """Entry point for post monitoring steps"""

        # Dump config file information (for debugging)
        if self.config['verbose']:
            self.dump_config()

        # We want to run these steps only on one node. We use master node
        if self.config['master_node'] == self.config['node_name']:
            # Run MergeFiles class
            merge_files = MergeFiles(config=self.config)
            merge_files.go()
