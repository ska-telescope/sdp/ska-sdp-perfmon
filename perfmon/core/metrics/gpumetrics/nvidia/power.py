"""Functions to monitor power related metrics for NVIDIA GPUs"""

import logging

from py3nvml.py3nvml import *

from perfmon.core.metrics.gpumetrics.nvidia import device_query

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def power_usage(data):
    """This method gets NVIDIA GPUs power usage metrics"""

    # Get power usage
    power_metrics = device_query('nvmlDeviceGetPowerUsage')
    for i, pow in enumerate(power_metrics):
        data[i]['power_usage'].append(pow)

    return data


def power_violation_report(data):
    """This method gets NVIDIA GPUs throttling period due to constraints"""

    # Get throttling due to all  constraints
    dev_throttling = device_query('nvmlDeviceGetViolationStatus', 10)
    for i, pow in enumerate(dev_throttling):
        data[i]['violation'].append(pow.violationTime)

    return data
