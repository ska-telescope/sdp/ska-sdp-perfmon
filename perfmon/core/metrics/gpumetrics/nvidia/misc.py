"""Functions to monitor misc metrics like temperature, fan speed for NVIDIA GPUs"""

import logging

from py3nvml.py3nvml import *

from perfmon.core.metrics.gpumetrics.nvidia import device_query

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def misc_metrics(data):
    """This method gets misc NVIDIA GPU metrics"""

    # Get temperature of devices
    temp_metrics = device_query('nvmlDeviceGetTemperature', NVML_TEMPERATURE_GPU)
    for i, temperature in enumerate(temp_metrics):
        data[i]['temperature'].append(temperature)

    # Get fan speed
    fan_speed_metrics = device_query('nvmlDeviceGetFanSpeed')
    for i, speed in enumerate(fan_speed_metrics):
        data[i]['fan_speed'].append(speed)

    # Get num procs
    num_procs = device_query('nvmlDeviceGetComputeRunningProcesses')
    for i, procs in enumerate(num_procs):
        data[i]['num_procs'].append(len(procs))

    # Get PCI express send throughput
    pcie_send_metrics = device_query('nvmlDeviceGetPcieThroughput', 0)
    for i, thpt in enumerate(pcie_send_metrics):
        data[i]['pci_thgpt']['send'].append(thpt)

    # Get PCI express receive throughput
    pcie_recv_metrics = device_query('nvmlDeviceGetPcieThroughput', 1)
    for i, thpt in enumerate(pcie_recv_metrics):
        data[i]['pci_thgpt']['recv'].append(thpt)

    return data
