"""Functions to get GPU utilization rates"""

import logging

from py3nvml.py3nvml import *

from perfmon.core.metrics.gpumetrics.nvidia import device_query

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_gpu_mem_util_rates(data):
    """This method gets GPU and memory utilization rates"""

    # Get GPU and memory utilization rates
    util_metrics = device_query('nvmlDeviceGetUtilizationRates')
    for i, rate in enumerate(util_metrics):
        data[i]['utilization_rates']['gpu'].append(rate.gpu)
        data[i]['utilization_rates']['memory'].append(rate.memory)

    return data


def get_encoder_decoder_util_rates(data):
    """This method gets encoder and decoder utilization rates"""

    # Get encoder utilization rates
    util_metrics = device_query('nvmlDeviceGetEncoderUtilization')
    for i, rate in enumerate(util_metrics):
        data[i]['encoder_util'].append(rate[0])

    # Get decoder utilization rates
    util_metrics = device_query('nvmlDeviceGetDecoderUtilization')
    for i, rate in enumerate(util_metrics):
        data[i]['decoder_util'].append(rate[0])

    return data
