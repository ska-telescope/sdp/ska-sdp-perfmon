"""Functions to monitor CPU usage metrics"""

import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_cpu_time(procs):
    """This method gets cumulative CPU time from parent and its childs"""

    # CPU Time of the parent process
    cpu_time_pp = 0
    for proc in procs:
        cpu_time_pp += proc.cpu_times().user

    # Return cpu_time
    return cpu_time_pp


def get_cpu_percent(cpu_aggregation_interval, procs):
    """This method gives CPU percent of parent and its childs"""

    # Get CPU percent for the process and its childs
    proc_cpu = 0
    for proc in procs:
        proc_cpu += proc.cpu_percent(interval=cpu_aggregation_interval)

    return proc_cpu
