"""This file contains convenience wrapper around hardware metadata extractor"""

import sys
import logging

from perfmon.core.metadata import GetHWMetadata

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


def get_hw_metadata(global_config):
    """This is wrapper to get HW metadata information"""

    # Get software and hardware meta data
    collect_md = GetHWMetadata(config=global_config)
    try:
        _log.info('Collecting software and hardware metadata....')
        collect_md.collect()
        _log.info('Collection of software and hardware metadata finished')
    except Exception:
        _log.warning('Collection of software and hardware metadata failed', exc_info=sys.exc_info())
