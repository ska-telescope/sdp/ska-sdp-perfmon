"""This file contains classes to setup lock files related steps"""
import os
import time
import logging

from perfmon.common.utils.locks import FileLock

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class SetUpLockFiles(object):
    """Set up all the required steps related to lock files creation"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config

    def create_lock_file(self):
        """Creates a lock file"""

        _log.info('Creating a lock file')
        # Name of the lock file. Prepending with '.' to make it hidden
        self.config['lock_file'] = os.path.join(
            self.config['save_dir'], '.{}'.format(self.config['node_name'])
        )
        # If lock file is already there from previous runs, purge them
        lock_file_name = '.'.join([self.config['lock_file'], 'lock'])
        if os.path.isfile(lock_file_name):
            _log.info('Purging old lock file...')
            os.remove(lock_file_name)
        self.fl = FileLock(self.config['lock_file'], timeout=10)

    def acquire_lock(self):
        """Acquires lock by locking the created file"""

        _log.info('Locking the file')
        self.fl.acquire()

    def unlock(self):
        """Release lock file"""

        try:
            _log.info('Releasing the lock file')
            self.fl.release()
        except FileNotFoundError:
            _log.warning('Lock file already released')

    def lock(self):
        """Preprocessing steps of data collection"""

        # Creates a lock file. Lock is acquired in pre processing call
        self.create_lock_file()

        # Acquire lock
        self.acquire_lock()

        return self.config

    def shutdown(self):
        """Finish monitoring"""

        # Release lock file. End of monitoring.
        self.unlock()

        # Check if computations on all nodes has finished. The way we do it is to check
        # if the lock file exists for each host. If it exists, computations on other hosts
        # still going on.
        # Sometimes the name of the node in SLURM is not exactly same as hostname
        if self.config['master_node'] == self.config['node_name']:
            _log.info('Checking if all nodes finished the collection')
            for node in self.config['node_name_list']:
                lock_file = os.path.join(self.config['save_dir'], '.{}'.format(node))
                fl = FileLock(lock_file)
                while fl.lock_exists():
                    time.sleep(1)
