"""This file contains classes to setup directories creation related steps"""

import os
import logging

_log = logging.getLogger(__name__)

# pylint: disable=E0401,W0201,C0301


class SetUpDirs(object):
    """Set up all the required steps related to directory creation"""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        """Initialize setup"""

        self.config = config

    def prepare_dirs(self):
        """Make directories to save the results"""

        # Create a sub dir with prefix as name if given
        if self.config['prefix'] is not None:
            self.config['save_dir'] = os.path.join(self.config['save_dir'], self.config['prefix'])

        # Create a folder to write the metric data
        os.makedirs(self.config['save_dir'], exist_ok=True)

        # Create a sub directory to save data
        self.config['data_dir'] = os.path.join(self.config['save_dir'], 'metrics')
        os.makedirs(self.config['data_dir'], exist_ok=True)

        if self.config['gen_report']:
            # Create a sub directory to save plots
            self.config['plot_dir'] = os.path.join(self.config['save_dir'], 'plots')
            os.makedirs(self.config['plot_dir'], exist_ok=True)

    def prepare_file_paths(self):
        """Prepare file paths for different result files"""

        # Create a file path to save report
        self.config['report_path'] = os.path.join(
            self.config['save_dir'], f"job-report-{self.config['job_id']}.pdf"
        )

        # Create a file name for each node by appending job ID and hostname
        self.config['temp_path'] = {}

        # Make file path for GPU metrics too
        for metric in self.config['metrics']:
            temp_path = os.path.join(self.config['save_dir'], 'raw_data', metric)
            os.makedirs(temp_path, exist_ok=True)
            self.config['temp_path'][metric] = temp_path

        # Perf event file path
        self.config['perf_event_file'] = os.path.join(
            self.config['save_dir'], '.perf_event_list.json'
        )

    def go(self):
        """Preprocessing steps of directory creation"""

        # Make directories to save results
        self.prepare_dirs()

        # Make file paths for different results
        self.prepare_file_paths()

        return self.config
