#!/usr/bin/env python

import os
import shutil
import sys
import glob
from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext

import subprocess

# Bail on Python < 3
assert sys.version_info[0] >= 3

# Get version information
version = {}
VERSION_PATH = os.path.join('perfmon', '_version.py')
with open(VERSION_PATH, 'r') as file:
    exec(file.read(), version)

# Load local readme file
with open("README.md") as readme_file:
    readme = readme_file.read()

# List of all packages
packages = [
    "perfmon",
]
packages += [
    i for p in packages for i in glob.glob(p + "/*/") +
                                 glob.glob(p + "/*/*/") +
                                 glob.glob(p + "/*/*/*/") +
                                 glob.glob(p + "/*/*/*/*/") +
                                 glob.glob(p + "/*/*/*/*/")
    if '__pycache__' not in i
]

package_data = {
    'perfmon': [
        'perfevents/*.yml'
    ]
}

# List of all required packages
reqs = [
    line.strip()
    for line in open("requirements.txt").readlines()
]

# A CMakeExtension needs a sourcedir instead of a file list.
# The name must be the _single_ output extension from the CMake build.
# If you need multiple extensions, see scikit-build.
class CMakeExtension(Extension):
    def __init__(self, name, sourcedir="src"):
        Extension.__init__(self, name, sources=[])
        if os.path.exists(f'external/{name}/src'):
            shutil.rmtree(f'external/{name}/src')

        if name == "pybind11":
            subprocess.check_call(['git', 'clone', 'https://github.com/pybind/pybind11.git', f'external/{name}/src'])
        elif name == "libpowersensor":
            subprocess.check_call(['git', 'clone', '--recursive', 'https://gitlab.com/astron-misc/libpowersensor.git', f'external/{name}/src'])
        self.sourcedir = os.path.abspath("external/{}/src")
        self.install_dir = os.path.abspath("external/{}/install")


class CMakeBuild(build_ext):

    def build_extension(self, ext):
        self.ext = ext
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        if not extdir.endswith(os.path.sep):
            extdir += os.path.sep


        for ext_name in self.ext_map:
            if ext_name == "pybind11":
                self.build_pybind11(extdir, self.ext.sourcedir.format(ext_name))
            elif ext_name == "libpowersensor":
                self.build_libpowersensor(extdir, self.ext.sourcedir.format(ext_name))

    def build_pybind11(self, extdir, sourcedir):
        debug = int(os.environ.get("DEBUG", 0)) if self.debug is None else self.debug
        # cfg = "Debug" if debug else "MinSizeRel"
        cfg = "Debug"

        cmake_args = [
            f"-DDOWNLOAD_CATCH=1",
            f"-DDOWNLOAD_EIGEN=1",
            f"-DPYBIND11_FINDPYTHON=1",

            f"-DCMAKE_LIBRARY_OUTPUT_DIRECTORY={extdir}",
            f"-DCMAKE_BUILD_TYPE={cfg}",  # not used on MSVC, but no harm
            f"-DCMAKE_INSTALL_PREFIX={self.ext.install_dir.format('pybind11')}",
        ]

        # Adding CMake arguments set as environment variable
        # (needed e.g. to build for ARM OSx on conda-forge)
        if "CMAKE_ARGS" in os.environ:
            cmake_args += [item for item in os.environ["CMAKE_ARGS"].split(" ") if item]

        if not os.path.exists(self.build_temp + "/pybind11"):
            os.makedirs(self.build_temp + "/pybind11")

        subprocess.check_call(
            ["cmake", sourcedir] + cmake_args, cwd=self.build_temp + "/pybind11"
        )
        subprocess.check_call(
            ["cmake", "--build", ".", "-j"], cwd=self.build_temp + "/pybind11"
        )
        subprocess.check_call(
            ['cmake', '--install', '.'], cwd=self.build_temp + "/pybind11"
        )

    def build_libpowersensor(self, extdir, sourcedir):
        debug = int(os.environ.get("DEBUG", 0)) if self.debug is None else self.debug
        cfg = "Debug" if debug else "Release"

        print(f"DEBUG: {self.ext.install_dir.format('pybind11')}/share/cmake/pybind11")
        cmake_args = [
            # needs pybind11, either installed via spack (spack install py-pybind11)
            # or directly on the system (apt install pybind11-dev)
            f"-DBUILD_PYTHON_POWERSENSOR=1",

            f"-DCMAKE_LIBRARY_OUTPUT_DIRECTORY={extdir}",
            f"-DCMAKE_BUILD_TYPE={cfg}",  # not used on MSVC, but no harm
            f"-DBUILD_NVML_POWERSENSOR=1",
            f"-DBUILD_LIKWID_POWERSENSOR=1",
            f"-DBUILD_ROCM_POWERSENSOR=1",

            f"-DCMAKE_PREFIX_PATH={self.ext.install_dir.format('pybind11')}/share/cmake/pybind11",
            
            f"-DCMAKE_INSTALL_PREFIX={self.ext.install_dir.format('libpowersensor')}",
        ]

        # Adding CMake arguments set as environment variable
        # (needed e.g. to build for ARM OSx on conda-forge)
        if "CMAKE_ARGS" in os.environ:
            cmake_args += [item for item in os.environ["CMAKE_ARGS"].split(" ") if item]

        if not os.path.exists(self.build_temp + "/libpowersensor"):
            os.makedirs(self.build_temp + "/libpowersensor")

        subprocess.check_call(
            ["cmake", sourcedir] + cmake_args, cwd=self.build_temp + "/libpowersensor"
        )
        subprocess.check_call(
            ["cmake", "--build", "."], cwd=self.build_temp + "/libpowersensor"
        )
        subprocess.check_call(
            ['cmake', '--install', '.'], cwd=self.build_temp + "/libpowersensor"
        )




# setup config
setup(
    name="ska-sdp-perfmon",
    version=version['__version__'],
    python_requires=">=3.7",
    description="A toolkit to monitor performance metrics for the jobs submitted using workload "
                "manager like SLURM",
    long_description=readme + "\n\n",
    maintainer="Mahendra Paipuri",
    maintainer_email="mahendra.paipuri@inria.fr",
    url="https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon",
    project_urls={
        "Documentation": "https://developer.skao.int/projects/ska-telescope-sdp-workflows-performance-monitoring/en/latest/",
        "Source": "https://gitlab.com/ska-telescope/sdp/ska-sdp-perfmon",
    },
    zip_safe=False,
    classifiers=[
        "Development Status :: Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    ext_modules=[CMakeExtension('pybind11'), CMakeExtension('libpowersensor')],
    cmdclass={"build_ext": CMakeBuild},
    packages=packages,
    package_data=package_data,
    scripts=['bin/perfmon'],
    install_requires=reqs,
)
