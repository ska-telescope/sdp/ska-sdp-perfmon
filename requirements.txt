# List of required python packages to run the benchmark script
archspec>=0.1.2
fastparquet>=0.6.0
feather-format~=0.4.1
fpdf2>=2.3.0
matplotlib>=3.3.0
pandas>=1.2.3
psutil>=5.8.0
py3nvml~=0.2.6
pyarrow>=5.0.0
PyYAML>=5.4
tables~=3.6.1
# pypowersensor via 'setup.py install'